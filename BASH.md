# How to BASH

### 0. Quick setup using this konfig

1. `mkdir $HOME/.config/bash`
2. `cd $HOME/.config/bash`
3. Clone this repo, or copy `konfig/user/bash` contents into your `$HOME/.config/bash`
4. Clone [ble.sh](https://github.com/akinomyoga/ble.sh) into your `$HOME/.config/bash`
    - `git clone --recursive --depth 1 --shallow-submodules https://github.com/akinomyoga/ble.sh.git`
    - `make -C ble.sh`
5. `cd $HOME`
6. `cp .bashrc .bashrc.bak`
7. `ln -s $HOME/.config/bash/.bashrc .bashrc`


### 1. Vi mode
1. `vim $HOME/.bashrc`
2. Add following lines
```bash
set -o vi
```

### 2. Autocomplete, Autosuggestion
1. `mkdir $HOME/.config/bash`
2. `cd $HOME/.config/bash`
3. Clone [ble.sh](https://github.com/akinomyoga/ble.sh)
    - `git clone --recursive --depth 1 --shallow-submodules https://github.com/akinomyoga/ble.sh.git`
    - `make -C ble.sh`
4. `touch $HOME/.config/bash/ble-cfg.sh`
5. Add following lines in `$HOME/.bashrc`
```bash
if [ -f "$HOME/.config/bash/ble.sh/out/ble.sh" ]; then
    . "$HOME/.config/bash/ble.sh/out/ble.sh"
    . "$HOME/.config/bash/ble-cfg.sh"
fi
```

### 3. Git prompt status
1. Copy this file from git repo into `$HOME/.config/bash`
```
https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh
```
2. `touch $HOME/.config/bash/git-cfg.sh`
3. Add following lines in `$HOME/.bashrc`
```bash
if [ -f "$HOME/.config/bash/git-prompt.sh" ]; then
  . "$HOME/.config/bash/git-prompt.sh"
  . "$HOME/.config/bash/git-cfg.sh"
fi

```
