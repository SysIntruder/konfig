# Konfig

My personal i3 rice, color scheme based on material design

preview:

![sample](./assets/sample.png)

## Setup

1. install apps listed in [app-list](./app-list.md)
2. configuration in [user](./user) goes to `$HOME/.config`
3. configuration in [system](./system) goes to `/etc` or `/`
4. keybind list [here](./keybind.md)
5. enable `lightdm.service` if you haven't yet

## Notes

- for **lightdm-mini-greeter** change `$USER` into your username
- you can find wallpaper [here](./assets/backgrounds)
- remapped action button cause some pc needs to press fn key (no need for mod key too)

## AMD settings

### fix backlight

1. add `acpi_backlight=amdgpu_bl0` in `/etc/default/grub`
2. update grub

### fix display not found

1. install `xf86-video-amdgpu xf86-video-ati`
2. follow [KMS early start](https://wiki.archlinux.org/title/Kernel_mode_setting#Early_KMS_start)
