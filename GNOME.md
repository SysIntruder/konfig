# How to GNOME

## Prerequisities

1. Install **GNOME Tweak Tools**
2. Install **User Themes** Extensions
3. Create gdm profile
    - `sudo mkdir /etc/dconf/profile`
    - `sudo touch /etc/dconf/profile/gdm`
    - `sudo nvim /etc/dconf/profile/gdm`
    - Add following lines
    ```
    user-db:user
    system-db:gdm
    file-db:/usr/share/gdm/greeter-dconf-defaults
    ```

### 1. Installing themes

1. Copy theme into `/usr/share/themes/` or `~/.themes/`
2. Change theme via **GNOME Tweak Tools**
3. Or run following command
```
gsettings set org.gnome.desktop.interface gtk-theme "theme-name"
gsettings set org.gnome.desktop.wm.preferences theme "theme-name"
```
4. For gtk-4.0 copy or symlink all files inside `theme-name/gtk-4.0/` into `~/.config/gtk-4.0/`
5. Set shell themes from **User Themes** Extensions

### 2. Installing icons and cursor

1. Copy icon/cursor into `/usr/share/icons/`
2. Change icon/cursor via **GNOME Tweak Tools**
3. Follow this steps to change GDM icon/cursor
    1. Login as gdm
    ```
    machinectl shell gdm@ /bin/bash
    ```
    2. Change cursor theme
    ```
    dbus-launch gsettings set org.gnome.desktop.interface cursor-theme 'cursor-name'
    ```
    3. Change icon theme
    ```
    dbus-launch gsettings set org.gnome.desktop.interface icon-theme 'cursor-name'
    ```
