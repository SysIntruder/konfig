# App List

App used in this konfig based on my preferences

## Themes
- bibata-cursor-theme
- materia-gtk-theme
- kvantum-theme-materia
- papirus-icon-theme
- ttf-joypixels (emoticons-fonts)
- ttf-roboto (ui-fonts)
- nerd-fonts-jetbrains-mono (terminal-fonts) (AUR)

## Shell
- zsh
- zsh-completions
- zsh-autosuggestions
- zsh-syntax-highlighting
- spaceship-prompt

## Terminal
- kitty

## Browser
- firefox
- chromium
- qutebrowser

## Utils
- exa (AUR)
- feh
- xwinwrap
- gotop-bin (AUR)
- htop
- acpi
- downgrade (AUR)
- bat (AUR)
- tty-clock (AUR)
- shell-color-scripts (AUR)

## Notification
- dunst
- libnotify

## Lock Screen
- xss-lock
- i3lock-color (AUR)
- multilockscreen (AUR)

## Login
- lightdm
- lightdm-mini-greeter (AUR)

## Polkit
- lxsession

## Bar
- polybar

## Media
- mpd
- mpc
- ncmpcpp
- mpv

## Extra
- neofetch
- lxappearance
- qt5ct
- kvantum-qt5
- picom-jonaburg-git (AUR)
- cava (AUR)
- maim
- simplescreenrecorder
- leafpad
- ranger
- plugins ranger_devicons
- virt-manager
- python-pillow (ranger image preview)

## Vim
- vim
- vim-airline
- vim-airline-themes
- vim-ale
- vim-easymotion
- vim-editorconfig
- vim-fugitive
- vim-gitgutter
- vim-nerdcommenter
- vim-nerdtree
- vim-seti
- vim-surround
- vim-syntastic
- vim-ultisnips
- vim-vital

## Neovim
- neovim
- plug (vim plugin manager)
- neovim (npm i -g)
- pyright (npm i -g)
- sql-language-server (npm i -g)
- typescript-language-server (npm i -g)
- typescript (npm i -g)
- vls (npm i -g)
- vscode-langservers-extracted (npm i -g)
- gopls (go install)
- phpactor (git clone)

## For AMD users
- amd-ucode
- xf86-video-amdgpu
- xf86-video-ati
- backlight_control (AUR)
