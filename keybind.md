# Keybind List

## i3-gaps

key | action
----|-------
mod | Super
<br>

### Workspace

key             | action 
----------------|-------------------------
h/left          | focus left
j/down          | focus down
k/up            | focus up
l/right         | focus right
Shift+h/left    | move left
Shift+j/down    | move down
Shift+k/up      | move up
Shift+l/right   | move right
,               | split horizontal
.               | split vertical
f               | fullscreen mode
[               | layout tabbed
]               | layout stacking
\               | layout split
Shift+space     | toggle float
space           | focus float / tiling
m               | focus parent
n               | focus child
`               | workspace before / after
PgUp            | prev workspace
PgDown          | next workspace
(1-8)           | change workspace
Shift+(1-8)     | switch to workspace
<br>

### Utils

key         | action
------------|-----------------------
Shift+c     | reload i3 config
Shift+r     | restart i3
Shift+e     | exit i3
9           | lockscreen
v           | kill picom
b           | start picom
t           | restart dunst
c           | manage service
w           | animated wallpaper on
Shift+w     | animated wallpaper off
<br>

### System

key             | action
----------------|-----------------
VolUp/F3        | volume up
VolDown/F2      | volume down
AudioMute/F1    | mute
MicMute/F4      | mic mute
Next/ >         | next music
Previous/ <     | prev music
PausePlay/ ?    | toggle music
BrightUp/F12    | brightness up
BrightDown/F11  | brightness down
<br>

### Application

key             | action
----------------|-------------------
Return          | terminal
Shift+Return    | floating terminal
F1              | vscodium
F2              | librewolf
F3              | pcmanfm
F4              | ncmpcpp
F5              | leafpad
F6              | krita
Shift+F2        | qutebrowser
Shift+F3        | vifm
Shift+F4        | cava
Shift+F5        | vim
Shift+F6        | inkscape
a               | alsamixer
d               | rofi
Tab             | network manager
Print           | screenshot
Shift+Print     | selection screenshot
q               | close
x               | kill
<br>

### Mode

key | action
----|-------
0   | system
r   | resize
g   | gaps
<br>

## kitty

key | action
----|-----------
mod | Ctrl+Shift
<br>

### Window

key | action
----|-------------------
w   | close
\-  | decrease font size
\+  | increase font size
<br>

### Tab

key | action
----|-------------
t   | new tab
h   | previous tab
l   | next tab
<br>

### Link
key     | action
--------|----------
click   | open link
<br>
