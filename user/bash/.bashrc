#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# no duplicate history
HISTCONTROL=ignoreboth:erasedups

# default prompt
PS1='[\e[34m\w\e[0m]> '
PS2='> '

# alias
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias rm='rm -i'
alias fetch='inxi -SBCGNDI'

# function
curlget() {
  curl -X GET "localhost$1" | jq
}
curlpost() {
  curl -X POST "localhost$1" -H 'Content-Type: application/json' -d "$2" | jq
}
curlpostbin() {
  curl -X POST "localhost$1" -H 'Content-Type: application/json' -d "@$2" | jq
}
curlput() {
  curl -X PUT "localhost$1" -H 'Content-Type: application/json' -d "$2" | jq
}
curlputbin() {
  curl -X PUT "localhost$1" -H 'Content-Type: application/json' -d "@$2" | jq
}
curldelete() {
  curl -X DELETE "localhost$1" | jq
}

mergehist() {
  cat "$1" "$2" | sort -n | uniq > .bash_history
}

# shell vi mode
set -o vi

# shell auto cd
shopt -s autocd

# shell completion ignore case
bind -s 'set completion-ignore-case on'

# set go
export PATH="/home/rei/go/bin:$PATH"

# set nvm
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# set turso
export PATH="/home/rei/.turso:$PATH"

# load git prompt
if [ -f "$HOME/.config/bash/git-prompt.sh" ]; then
  . "$HOME/.config/bash/git-prompt.sh"
  . "$HOME/.config/bash/git-cfg.sh"
fi

# remove any autocompletion idk they said it makes autocompletion faster
complete -r

# autostart tmux
if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
  tmux a -t default || exec tmux new -s default && exit;
fi
