function my/vim-load-hook {
  bleopt keymap_vi_mode_string_nmap=$'\e[1m-- NORMAL --\e[m'
  source "$_ble_base/lib/vim-surround.sh"
}

blehook/eval-after-load keymap_vi my/vim-load-hook
