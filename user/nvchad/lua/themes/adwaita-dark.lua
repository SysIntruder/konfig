-- based on https://github.com/NvChad/base46/blob/v2.5/lua/base46/themes/github_dark.lua
-- Adwaita color palette https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/named-colors.html

local M = {}

M.base_30 = {
  white = "#FAFAFA",
  darker_black = "#191919", -- telescope bg
  black = "#1E1E1E", --  nvim bg, active buffer, statusline mode
  black2 = "#242424", -- selection, buffer bg
  one_bg = "#242424", -- StatusBar icon
  one_bg2 = "#303030", -- StatusBar (filename)
  one_bg3 = "#303030", -- StatusBar light bg
  grey = "#5E5C64", -- Line numbers (shouldn't be base01?)
  grey_fg = "#5E5C64", -- Why this affects comments?
  grey_fg2 = "#5E5C64",
  light_grey = "#5E5C64", -- Inactive buffer text
  red = "#ED333B", -- StatusBar (username)
  baby_pink = "#F66151",
  pink = "#DC8ADD",
  line = "#303030", -- for lines like vertsplit
  green = "#57E389", -- Command Mode indicator
  vibrant_green = "#8FF0A4",
  nord_blue = "#99C1F1", -- Normal Mode indicator
  blue = "#62A0EA",
  yellow = "#F8E45C",
  sun = "#F9F06B",
  purple = "#C061CB", -- Insert Mode indicator
  dark_purple = "#9141AC",
  teal = "#0AB9DC",
  orange = "#FFA348", -- Replace Mode indicator
  cyan = "#4FD2FD", -- Visual Mode indicator
  statusline_bg = "#1E1E1E",
  lightbg = "#303030",
  pmenu_bg = "#62A0EA", -- Command bar suggestions
  folder_bg = "#62A0EA",
}

M.base_16 = {
  base00 = "#1E1E1E", -- Default bg
  base01 = "#242424", -- Lighter bg (status bar, line number, folding mks)
  base02 = "#303030", -- Selection bg
  base03 = "#5E5C64", -- Comments, invisibles, line hl
  base04 = "#C0BFBC", -- Dark fg (status bars)
  base05 = "#EBEBEB", -- Default fg (caret, delimiters, Operators)
  base06 = "#FAFAFA", -- Light fg (not often used)
  base07 = "#FFFFFF", -- Light bg (not often used)
  base08 = "#ED333B", -- Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
  base09 = "#FFA348", -- Integers, Boolean, Constants, XML Attributes, Markup Link Url
  base0A = "#F8E45C", -- Classes, Markup Bold, Search Text Background
  base0B = "#57E389", -- Strings, Inherited Class, Markup Code, Diff Inserted
  base0C = "#4FD2FD", -- Support, regex, escape chars
  base0D = "#62A0EA", -- Function, methods, headings
  base0E = "#C061CB", -- Keywords
  base0F = "#0AB9DC", -- Deprecated, open/close embedded tags
}

M.type = "dark"

return M
