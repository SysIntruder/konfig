-- based on https://github.com/NvChad/base46/blob/v2.5/lua/base46/themes/github_dark.lua
-- Adwaita color palette https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/named-colors.html

local M = {}

M.base_30 = {
  white = "#1E1E1E",
  darker_black = "#FFFFFF", -- telescope bg
  black = "#FAFAFA", --  nvim bg, active buffer, statusline mode
  black2 = "#EBEBEB", -- selection, buffer bg
  one_bg = "#EBEBEB", -- StatusBar icon
  one_bg2 = "#D0D0D0", -- StatusBar (filename)
  one_bg3 = "#D0D0D0", -- StatusBar light bg
  grey = "#C0BFBC", -- Line numbers (shouldn't be base01?)
  grey_fg = "#C0BFBC", -- Why this affects comments?
  grey_fg2 = "#C0BFBC",
  light_grey = "#C0BFBC", -- Inactive buffer text
  red = "#A51D2D", -- StatusBar (username)
  baby_pink = "#C01C28",
  pink = "#DC8ADD",
  line = "#D0D0D0", -- for lines like vertsplit
  green = "#26A269", -- Command Mode indicator
  vibrant_green = "#2EC27E",
  nord_blue = "#1C71D8", -- Normal Mode indicator
  blue = "#1A5FB4",
  yellow = "#E5A50A",
  sun = "#F5C211",
  purple = "#9141AC", -- Insert Mode indicator
  dark_purple = "#813D9C",
  teal = "#4FD2FD",
  orange = "#C64600", -- Replace Mode indicator
  cyan = "#0AB9DC", -- Visual Mode indicator
  statusline_bg = "#FAFAFA",
  lightbg = "#D0D0D0",
  pmenu_bg = "#1A5FB4", -- Command bar suggestions
  folder_bg = "#1A5FB4",
}

M.base_16 = {
  base00 = "#FAFAFA", -- Default bg
  base01 = "#EBEBEB", -- Lighter bg (status bar, line number, folding mks)
  base02 = "#D0D0D0", -- Selection bg
  base03 = "#C0BFBC", -- Comments, invisibles, line hl
  base04 = "#5E5C64", -- Dark fg (status bars)
  base05 = "#303030", -- Default fg (caret, delimiters, Operators)
  base06 = "#242424", -- Light fg (not often used)
  base07 = "#191919", -- Light bg (not often used)
  base08 = "#A51D2D", -- Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
  base09 = "#C64600", -- Integers, Boolean, Constants, XML Attributes, Markup Link Url
  base0A = "#E5A50A", -- Classes, Markup Bold, Search Text Background
  base0B = "#26A269", -- Strings, Inherited Class, Markup Code, Diff Inserted
  base0C = "#0AB9DC", -- Support, regex, escape chars
  base0D = "#1A5FB4", -- Function, methods, headings
  base0E = "#9141AC", -- Keywords
  base0F = "#4FD2FD", -- Deprecated, open/close embedded tags
}

M.type = "light"

return M
