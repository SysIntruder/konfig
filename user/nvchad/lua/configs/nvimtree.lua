local options = require "nvchad.configs.nvimtree"

options.view.number = true
options.view.relativenumber = true
options.view.width = 35

options.git.ignore = false

options.diagnostics = {
  enable = true,
  show_on_dirs = true,
}

options.filters.exclude = {
  ".gitignore",
}
options.filters.custom = {
  ".git",
}

require("nvim-tree").setup(options)
