return {
  provider_selector = function(_, ft, _)
    local lspWithOutFolding = { "markdown", "sh", "css", "html", "python" }
    if vim.tbl_contains(lspWithOutFolding, ft) then
      return { "treesitter", "indent" }
    end
    return { "lsp", "indent" }
  end,
  close_fold_kinds_for_ft = {
    default = { "comment" },
  },
  open_fold_hl_timeout = 800,
}
