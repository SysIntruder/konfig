require "nvchad.options"

local o = vim.o
local opt = vim.opt
local ftype = vim.filetype

o.relativenumber = true
o.autoindent = true
o.smartindent = true
o.cursorlineopt = "both"
o.scrolloff = 5
o.cc = 120

opt.shortmess:append {
  s = true,
  I = false,
}

o.foldlevel = 99
o.levelstart = 99
o.foldenable = true

ftype.add { extension = { templ = "templ" } }
