require "nvchad.mappings"

local map = vim.keymap.set
local nomap = vim.keymap.del

map("n", "<leader>ft", "<cmd>Telescope treesitter<CR>", { desc = "Telescope Treesitter" })
map("n", "<leader>da", "<cmd>Telescope diagnostics<CR>", { desc = "Telescope Lsp Diagnostics" })
map("n", "<leader>ds", "<cmd>Telescope lsp_document_symbols<CR>", { desc = "Telescope Document Symbols" })

local ufo = require "ufo"

map("n", "zR", function()
  ufo.openAllFolds()
end, { desc = "Ufo Open All Folds" })
map("n", "zr", function()
  ufo.openFoldsExceptKinds { "comment", "imports" }
end, { desc = "Ufo Open Regular Folds" })
map("n", "zM", function()
  ufo.closeAllFolds()
end, { desc = "Ufo Close All Folds" })

map("n", "<leader>lg", "<cmd>LazyGit<CR>", { desc = "LazyGit toggle" })

map("n", "<leader>X", function()
  require("nvchad.tabufline").closeAllBufs()
end, { desc = "Buffer Close All" })

-- GO Specific
map("n", "<leader>gTdj", function()
  require("gomodifytags").GoAddTags("json,db", { transformation = "snakecase", skip_unexported = false })
end, { desc = "Gomodifytags add json and db tag" })
map("n", "<leader>gTd", function()
  require("gomodifytags").GoAddTags("db", { transformation = "snakecase", skip_unexported = false })
end, { desc = "Gomodifytags add db tag" })
map("n", "<leader>gTj", function()
  require("gomodifytags").GoAddTags("json", { transformation = "snakecase", skip_unexported = false })
end, { desc = "Gomodifytags add json tag" })
map("n", "<leader>gTq", function()
  require("gomodifytags").GoAddTags("query", { transformation = "snakecase", skip_unexported = false })
end, { desc = "Gomodifytags add query tag" })
map("n", "<leader>gTf", function()
  require("gomodifytags").GoAddTags("form", { transformation = "snakecase", skip_unexported = false })
end, { desc = "Gomodifytags add form tag" })

nomap("t", "<ESC>")
