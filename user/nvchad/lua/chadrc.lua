local M = {}

M.ui = {
  theme = "adwaita-dark",
  theme_toggle = { "adwaita-dark", "adwaita-light" },
  transparency = false,

  statusline = {
    theme = "default",
    separator_style = "block",
  },

  tabufline = {
    lazyload = false,
  },

  hl_override = {
    Comment = { italic = true },
    ["@comment"] = { italic = true },

    NvimTreeGitNew = { fg = "green" },
    NvimTreeGitDirty = { fg = "yellow" },
  },
}

return M
