" Plugin
source $HOME/.config/nvim/plug.vim

" Keymap
source $HOME/.config/nvim/map.vim

" Lua config
source $HOME/.config/nvim/lua.vim

filetype plugin indent on	" Auto-indent depend on filetype

set autoindent              " New line indentation
set clipboard+=unnamedplus	" Use system clipboard
set hlsearch                " Enable search highlighting
set mouse=a                 " Enable mouse
set noshowmode              " Hide mode
set noshowcmd	            " Hide last cmd
set number					" Show line numbers
set relativenumber			" Show relative numbers
set shiftwidth=4            " Use 4 spaces
set smartindent				" Smart indentation
set softtabstop=4           " 4 spaces as tabs
set tabstop=4				" Tabs = 4 spaces

" Run xrdb after editting Xresources
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

" Python3 support
let g:python3_host_prog = '/usr/bin/python3'

" nvim-completion snippet possible value: 'UltiSnips', 'Neosnippet', 'vim-vsnip', 'snippets.nvim'
let g:completion_enable_snippet = 'UltiSnips'

" NERDCommenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1

" NERDTree enable line numbers
let NERDTreeShowLineNumbers=1
autocmd FileType nerdtree setlocal relativenumber

" NERDTree options
let g:NERDTreeShowHidden = 1
let g:NERDTreeMinimalUI = 1
let g:NERDTreeIgnore = []
let g:NERDTreeStatusline = ''

" Automaticaly close nvim if NERDTree is only thing left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Open NERDTree when no file specified
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
	\ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | wincmd p | endif

" Open new split panes to right and below
set splitright
set splitbelow

" Start terminal in insert mode
au BufEnter * if &buftype == 'terminal' | :startinsert | endif

