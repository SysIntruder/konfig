call plug#begin('~/.config/nvim/plugged')

" Side explorer
Plug 'preservim/nerdtree'

" Explorer icons
Plug 'ryanoasis/vim-devicons'

" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Telescope fzf
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Git gutter
Plug 'airblade/vim-gitgutter'

" LSP
Plug 'neovim/nvim-lspconfig'
Plug 'glepnir/lspsaga.nvim'

" PHPactor
Plug 'phpactor/phpactor', {'for': 'php', 'tag': '*', 'do': 'composer install --no-dev -o'}

" Completion-nvim
Plug 'nvim-lua/completion-nvim'
Plug 'cohama/lexima.vim'

Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'

" Commenter
Plug 'preservim/nerdcommenter'

" Status line
Plug 'hoob3rt/lualine.nvim'

call plug#end()
