lua <<EOF

-- Telescope fzf
local actions = require('telescope.actions')require('telescope').setup{
  defaults = {
    mappings = {
      n = {
        ["q"] = actions.close
      },
    },
  }
}

-- LSP config
local nvim_lsp = require('lspconfig')

local on_attach = function(client, bufnr)
	require('completion').on_attach(client, buffer)
end

 nvim_lsp.gopls.setup{
		on_attach = on_attach,
		cmd = { "gopls" }
	}
 nvim_lsp.phpactor.setup{ on_attach = on_attach }
 nvim_lsp.vuels.setup{ on_attach = on_attach }
 nvim_lsp.html.setup{ on_attach = on_attach }
 nvim_lsp.cssls.setup{ on_attach = on_attach }
 nvim_lsp.jsonls.setup {
	 on_attach = on_attach,
     commands = {
       Format = {
         function()
           vim.lsp.buf.range_formatting({},{0,0},{vim.fn.line("$"),0})
         end
       }
     }
 }
 nvim_lsp.tsserver.setup{ on_attach = on_attach }
 nvim_lsp.sqlls.setup{ on_attach = on_attach }
 nvim_lsp.pyright.setup{ on_attach = on_attach }

-- LSPsaga
local saga = require('lspsaga')
saga.init_lsp_saga {
  error_sign = '',
  warn_sign = '',
  hint_sign = '',
  infor_sign = '',
  border_style = "round",
}

-- Treesitter
require('nvim-treesitter.configs').setup {
  highlight = {
    enable = true,
    disable = {},
  },
  indent = {
    enable = false,
    disable = {},
  },
  ensure_installed = {
    "javascript",
    "typescript",
    "html",
    "css",
    "scss",
    "php",
    "go"
  }
}

local parser_config = require('nvim-treesitter.parsers').get_parser_configs()
parser_config.tsx.used_by = { "javascript", "typescript.tsx" }

-- Status line
local status, lualine = pcall(require, "lualine")
if (not status) then return end

lualine.setup {
  options = {
    icons_enabled = true,
    theme = '16color',
    section_separators = {'', ''},
    component_separators = {'', ''},
    disabled_filetypes = {}
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch'},
    lualine_c = {'filename'},
    lualine_x = {
      { 'diagnostics', sources = {"nvim_lsp"}, symbols = {error = ' ', warn = ' ', info = ' ', hint = ' '} },
      'encoding',
      'filetype'
    },
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  extensions = {'fugitive'}
}

EOF
