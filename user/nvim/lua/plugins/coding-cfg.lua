return {
	-- Leap
	{
		"ggandor/leap.nvim",
		config = function()
			require("leap").create_default_mappings()
		end,
	},

	-- Fold
	{
		"kevinhwang91/nvim-ufo",
		dependencies = "kevinhwang91/promise-async",
		event = "BufReadPost",
		config = function()
			require("ufo").setup({
				provider_selector = function(_, ft, _)
					local lspWithOutFolding = { "markdown", "sh", "css", "html", "python" }
					if vim.tbl_contains(lspWithOutFolding, ft) then
						return { "treesitter", "indent" }
					end
					return { "lsp", "indent" }
				end,
				close_fold_kinds_for_ft = {
					default = { "comment" },
				},
				open_fold_hl_timeout = 800,
			})
		end,
	},

	-- File Manager
	{
		"stevearc/oil.nvim",
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
		config = function()
			require("oil").setup({
				default_file_explorer = true,
				view_options = {
					show_hidden = true,
				},
			})

			vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" })
		end,
	},
}
