" Toggle NERDTree
nnoremap <silent> <C-b> <Cmd>NERDTreeToggle<CR>

" Turn terminal to normal mode with escape
tnoremap <Esc> <C-\><C-n>

" Open terminal on ctrl+n
function! OpenTerminal()
  split term://zsh
  resize 10
endfunction
nnoremap <C-n> :call OpenTerminal()<CR>

" Telescope fzf keymap 
nnoremap <silent> ;f <Cmd>Telescope find_files<CR>
nnoremap <silent> ;r <Cmd>Telescope live_grep<CR>
nnoremap <silent> ;\ <Cmd>Telescope buffers<CR>
nnoremap <silent> ;/ <Cmd>Telescope help_tags<CR>

" Completion nvim
set completeopt=menuone,noinsert,noselect
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

let g:completion_confirm_key = ""
imap <expr> <cr>  pumvisible() ? complete_info()["selected"] != "-1" ?
                 \ "\<Plug>(completion_confirm_completion)"  : "\<c-e>\<CR>" :  "\<CR>"

" UltiSnips
let g:UltiSnipsExpandTrigger="<C-k>"
