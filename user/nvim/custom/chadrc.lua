---@type ChadrcConfig
local M = {}

M.ui = {
  theme = 'catppuccin',
  nvdash = {
    load_on_startup = true,
  },

  transparency = true,

  statusline = {
    theme = "minimal",
  },

  hl_override = {
    Visual = { bg = "grey_fg2" },
    Comment = { italic = true },
  },
}
M.plugins = "custom.plugins"
M.mappings = require("custom.mappings")

return M
