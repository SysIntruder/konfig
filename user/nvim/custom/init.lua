vim.opt.relativenumber = true
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.filetype.add({ extension = { templ = "templ" } })
