local none_ls = require("null-ls")
local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

local lsp_formatting = function (bufnr)
  vim.lsp.buf.format({
    async = false,
    filter = function (client)
      return client.name == "null-ls"
    end,
    bufnr = bufnr,
  })
end

local opts = {
  sources = {
    none_ls.builtins.formatting.gofmt,
    none_ls.builtins.formatting.goimports,
    none_ls.builtins.formatting.golines.with({
      condition = function()
        return not string.find(os.getenv("PWD"), "/srv/project")
      end
    }),

    -- none_ls.builtins.formatting.eslint,
    -- none_ls.builtins.diagnostics.eslint,
    -- none_ls.builtins.code_actions.eslint,
    require("none-ls.formatting.eslint"),
    require("none-ls.diagnostics.eslint"),
    require("none-ls.code_actions.eslint"),

    none_ls.builtins.formatting.clang_format,

    -- none_ls.builtins.formatting.rustfmt,
    require("none-ls.formatting.rustfmt"),

    -- none_ls.builtins.formatting.templ,
    -- require("none-ls.formatting.templ"),
  },
  on_attach = function (client, bufnr)
    if client.supports_method("textDocument/formatting") then
      vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
      vim.api.nvim_create_autocmd("BufWritePre", {
        group = augroup,
        buffer = bufnr,
        callback = function ()
          lsp_formatting(bufnr)
        end
      })
    end
  end
}

return opts
