local plugins = {
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "gopls",
        "goimports",
        "golines",
        "gomodifytags",
        "delve",

        "typescript-language-server",
        "eslint-lsp",
        "js-debug-adapter",
        "emmet-ls",
        "vue-language-server",

        "clangd",
        "clang-format",

        "rust-analyzer",

        "templ",
        "html-lsp",
        "htmx-lsp",
        "css-lsp",
        "tailwindcss-language-server",
      },
    },
  },
  {
    "neovim/nvim-lspconfig",
    config = function ()
      require("plugins.configs.lspconfig")
      require("custom.configs.lspconfig")
    end
  },
  {
    "nvimtools/none-ls.nvim",
    dependencies = {
      "nvimtools/none-ls-extras.nvim",
    },
    ft = { "go", "templ", "html", "css", "rust", "c", "cpp", "javascript", "typescript", "vue" },
    -- event = "VeryLazy",
    opts = function ()
      return require("custom.configs.none-ls")
    end,
  },
  {
    "mfussenegger/nvim-dap",
    enabled = false,
    config = function ()
      require "custom.configs.dap"
      require("core.utils").load_mappings("dap")
    end
  },
  {
    "leoluz/nvim-dap-go",
    enabled = false,
    ft = "go",
    dependencies = "mfussenegger/nvim-dap",
    config = function (_, opts)
      require("dap-go").setup(opts)
      require("core.utils").load_mappings("dap_go")
    end
  },
  {
    "rcarriga/nvim-dap-ui",
    enabled = false,
    event = "VeryLazy",
    dependencies = "mfussenegger/nvim-dap",
    config = function ()
      local dap = require("dap")
      local dapui = require("dapui")
      require("dapui").setup()
      dap.listeners.after.event_initialized["dapui_config"] = function ()
        dapui.open()
      end
      dap.listeners.before.event_terminated["dapui_config"] = function ()
        dapui.close()
      end
      dap.listeners.before.event_exited["dapui_config"] = function ()
        dapui.close()
      end
    end
  },
}
return plugins
