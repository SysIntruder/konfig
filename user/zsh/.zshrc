# Debug zsh
#zmodload zsh/zprof

# Spaceship Vi Mode
source "${HOME}/.config/spaceship/spaceship-vi-mode/spaceship-vi-mode.plugin.zsh"

spaceship add --after line_sep vi_mode
eval spaceship_vi_mode_enable
SPACESHIP_VI_MODE_INSERT=''
SPACESHIP_VI_MODE_NORMAL=''

# Lines configured by zsh-newuser-install
HISTFILE=$ZDOTDIR/.histfile
HISTSIZE=1000
SAVEHIST=1000

setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS

setopt notify autocd nomatch
unsetopt beep extendedglob notify

KEYTIMEOUT=1
VIM_MODE_NO_DEFAULT_BINDINGS=true
bindkey -v

# The following lines were added by compinstall
zstyle :compinstall filename "${ZDOTDIR}/.config/zsh/.zshrc"
zstyle ':completion:*' menu select

setopt COMPLETE_ALIASES

autoload -Uz compinit
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mH+24) ]]; then
    compinit
else
    compinit -C
fi

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

export NVM_DIR=~/.nvm
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

precmd () {print -Pn "\e]0;%~\a"}

colorscript -e crunchbang-mini

# Turso
export PATH="/home/rei/.turso:$PATH"
