;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = #88151515
background-alt = #424242
foreground = #F5F5F5
foreground-alt = #E0E0E0
primary = #42A5F5
secondary = #4DB6AC
alert = #EF5350

[section/base]
background = ${colors.background}
foreground = ${colors.foreground}

width = 100%
height = 25
radius = 0
fixed-center = true

offset-x = 5
offset-y = 5

override-redirect = false
wm-restack = i3

separator = %{T2}%{T-}

line-size = 0

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 0

module-margin-left = 0
module-margin-right = 0

font-0 = "JetBrainsMono Nerd Font:style=Regular:size=10;1"
font-1 = "JetBrainsMono Nerd Font:style=Regular:size=11;2"
font-2 = "Source Han Sans JP,源ノ角ゴシック JP:style=Regular:size=11;2"

tray-position = none
tray-padding = 0

cursor-click = pointer
cursor-scroll = ns-resize

[bar/top]
inherit = section/base
bottom = false

modules-left = i3 mpd
;modules-center = xwindow
modules-right = wlan eth cpu memory filesystem backlight pulseaudio battery date

[module/xwindow]
type = internal/xwindow
label = %title:0:40:...%

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

format-mounted-prefix = %{T2} %{T-}
format-mounted-prefix-foreground = ${colors.primary}
format-mounted-padding = 1

format-unmounted-prefix = %{T2} %{T-}
format-unmounted-prefix-foreground = ${colors.background-alt}
format-unmounted-padding = 1

label-mounted = %percentage_used%%
label-unmounted = root not mounted
label-unmounted-foreground = ${colors.background-alt}

[module/i3]
type = internal/i3
format = "<label-state><label-mode>"

index-sort = true
wrapping-scroll = false

ws-icon-0 = 1;%{T2}%{T-}
ws-icon-1 = 2;%{T2}%{T-}
ws-icon-2 = 3;%{T2}%{T-}
ws-icon-3 = 4;%{T2}%{T-}
ws-icon-4 = 5;%{T2}%{T-}
ws-icon-5 = 6;%{T2}%{T-}
ws-icon-6 = 7;%{T2}%{T-}
ws-icon-7 = 8;%{T2}%{T-}

label-mode = "%{T2}%{T-} %mode% "
label-mode-padding = 0
label-mode-foreground = ${colors.foreground}
;label-mode-background = ${colors.background-alt}

; focused = Active workspace on focused monitor
label-focused = %{T2}%icon%%{T-}
label-focused-foreground = ${colors.primary}
label-focused-padding = 1

; unfocused = Inactive workspace on any monitor
label-unfocused = %{T2}%icon%%{T-}
label-unfocused-padding = 1

; visible = Active workspace on unfocused monitor
label-visible = %{T2}%icon%%{T-}
label-visible-padding = 1

; urgent = Workspace with urgency hint set
label-urgent = %{T2}%icon%%{T-}
label-urgent-foreground = ${colors.alert}
label-urgent-padding = 1

[module/mpd]
type = internal/mpd
format-online = <icon-prev> <toggle> <icon-next> <label-song>
format-stopped = <icon-prev> <toggle> <icon-next> online

format-online-padding = 1
format-stopped-padding = 1
format-offline-padding = 1

icon-prev = %{T2} %{T-}
icon-stop = %{T2} %{T-}
icon-play = %{T2} %{T-}
icon-pause = %{T2} %{T-}
icon-next = %{T2} %{T-}

label-time = %elapsed%

label-song = %title%
label-song-maxlen = 50
label-song-ellipsis = true

label-offline = %{T2} %{T-}offline

[module/backlight]
type = internal/backlight
card = amdgpu_bl0

format = <label>
format-prefix = %{T2} %{T-}
format-prefix-foreground = ${colors.primary}
format-padding = 1

label = %percentage%%

[module/cpu]
type = internal/cpu
interval = 0.5
format-prefix = %{T2} %{T-}
format-prefix-foreground = ${colors.primary}
format-padding = 1
label = %percentage%%

[module/memory]
type = internal/memory
interval = 0.5
format-prefix = %{T2} %{T-}
format-prefix-foreground = ${colors.primary}
format-padding = 1
label = %percentage_used%%

[module/wlan]
type = internal/network
interface = wlp1s0
interval = 1.0

format-connected-prefix = %{T2} %{T-}
format-connected-prefix-foreground = ${colors.primary}
format-connected = <label-connected>
format-connected-padding = 1
label-connected = %essid% %{T2}%{T-} %{F#42A5F5}%{F-} %upspeed% %{T2}%{T-} %{F#42A5F5}%{F-} %downspeed%

format-disconnected = <label-disconnected>
format-disconnected-padding = 1
label-disconnected = %ifname% disconnected
label-disconnected-foreground = ${colors.background-alt}

[module/eth]
type = internal/network
interface = enp0s3
interval = 1.0

format-connected-prefix = %{T2} %{T-}
format-connected-prefix-foreground = ${colors.primary}
format-connected-padding = 1
label-connected = %ifname%

format-disconnected = <label-disconnected>
format-disconnected-padding = 1
label-disconnected = %ifname% disconnected
label-disconnected-foreground = ${colors.background-alt}

[module/date]
type = internal/date
interval = 1

date = 
date-alt = "%Y-%m-%d "

time = %H:%M
time-alt = %H:%M:%S

format-prefix = %{T2} %{T-}
format-prefix-foreground = ${colors.primary}
format-padding = 1
label = %date%%time%

[module/pulseaudio]
type = internal/pulseaudio
use-ui-max = false

format-volume-prefix = %{T2} %{T-}
format-volume-prefix-foreground = ${colors.primary}
format-volume-padding = 1
format-volume = <label-volume>
label-volume = %percentage%%

format-muted-padding = 1
label-muted = %{T2} %{T-}muted
label-muted-foreground = ${colors.background-alt}

[module/alsa]
type = internal/alsa

format-volume-prefix = %{T2} %{T-}
format-volume-prefix-foreground = ${colors.primary}
format-volume-padding = 1
format-volume = <label-volume>
label-volume = %percentage%%

format-muted-padding = 1
label-muted = %{T2} %{T-}muted
label-muted-foreground = ${colors.background-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP1
full-at = 98

format-charging-padding = 1
format-charging = <animation-charging><label-charging>

format-discharging-padding = 1
format-discharging = <animation-discharging><label-discharging>

format-full-padding = 1
format-full = <ramp-capacity><label-full>

ramp-capacity-0 = %{T2} %{T-}
ramp-capacity-1 = %{T2} %{T-}
ramp-capacity-2 = %{T2} %{T-}
ramp-capacity-foreground = ${colors.primary}

animation-charging-0 = %{T2} %{T-}
animation-charging-1 = %{T2} %{T-}
animation-charging-2 = %{T2} %{T-}
animation-charging-foreground = ${colors.primary}
animation-charging-framerate = 750

animation-discharging-0 = %{T2} %{T-}
animation-discharging-1 = %{T2} %{T-}
animation-discharging-2 = %{T2} %{T-}
animation-discharging-foreground = ${colors.primary}
animation-discharging-framerate = 750

[settings]
screenchange-reload = true

[global/wm]
margin-top = 5
margin-bottom = 5

